import React, { useEffect, useReducer, useState } from "react"
import Header from "../header/Header"
import Preloader from "../preloader/Preloader"
import "./Chat.css"
import MessageList from "../message_list/MessageList"
import { MessageData, MessagesAction, MessagesState } from "../../types"
import MessageInput from "../message_input/MessageInput"
import { CURRENT_USER_ID } from "../../constants/userId"
import {
  ADD_LIKE_ON_MESSAGE,
  ADD_NEW_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  LOAD_MESSAGES,
} from "../../constants/actions"

interface ChatProps {
  url: string
}

const initialMessagesState = { messages: [] }

const messagesReducer = (state: MessagesState, action: MessagesAction) => {
  // payload fields with ternary ?, because of types.ts MessagesAction
  switch (action.type) {
    case LOAD_MESSAGES:
      return { messages: action.payload.messages ? [...action.payload.messages] : [] }
    case ADD_NEW_MESSAGE:
      const newMessageCreatedAt = new Date()

      const newMessageData: MessageData = {
        id: Date.now().toString(),
        userId: CURRENT_USER_ID,
        avatar: "",
        user: "katapilleer",
        text: action.payload.messageText ? action.payload.messageText : "",
        createdAt: newMessageCreatedAt.toUTCString(),
        editedAt: "",
      }
      return { messages: [...state.messages, newMessageData] }
    case DELETE_MESSAGE:
      const messages = [...state.messages]
      const removeIndex = messages.findIndex(el => el.id === action.payload.messageId)

      messages.splice(removeIndex, 1)
      return { messages: messages }
    case EDIT_MESSAGE:
      const allMessages = [...state.messages]
      const editIndex = allMessages.findIndex(el => el.id === action.payload.messageId)
      const editedMessage = allMessages[editIndex]

      const messageEditedAt = new Date()
      editedMessage.text = action.payload.messageText ? action.payload.messageText : editedMessage.text
      editedMessage.editedAt = messageEditedAt.toUTCString()

      allMessages[editIndex] = editedMessage
      return { messages: allMessages }
    case ADD_LIKE_ON_MESSAGE:
      const messagesArr = [...state.messages]
      const likeMessageIndex = messagesArr.findIndex(el => el.id === action.payload.messageId)
      const likeMessage = messagesArr[likeMessageIndex]

      //if message was liked check
      if (!action.payload.isLiked) {
        const likedByArr = likeMessage.likedBy ? [...likeMessage.likedBy, CURRENT_USER_ID] : [CURRENT_USER_ID]
        likeMessage.likedBy = [...likedByArr]
      } else {
        const deleteIndex = likeMessage.likedBy!.indexOf(CURRENT_USER_ID)
        likeMessage.likedBy!.splice(deleteIndex, 1)
      }

      messagesArr[likeMessageIndex] = likeMessage
      return { messages: [...messagesArr] }
    default:
      return state
  }
}

const Chat = ({ url }: ChatProps) => {
  const [isLoaded, setIsLoaded] = useState(false)

  const [messagesState, dispatch] = useReducer(messagesReducer, initialMessagesState)

  //states for managing message editing
  const [inputtedMessage, setInputtedMessage] = useState("")
  const [isEditMessage, setIsEditMessage] = useState(false)
  const [editMessageId, setEditMessageId] = useState("")

  useEffect(() => {
    const fetchMessageData = async () => {
      setIsLoaded(false)
      try {
        const res = await fetch(url)
        const data = await res.json()
        dispatch({ type: LOAD_MESSAGES, payload: { messages: data } })
      } catch (error) {
        console.log(error)
      }
      setIsLoaded(true)
    }
    fetchMessageData()
  }, [url])

  const getChatParticipants = () => {
    const usersSet = new Set()
    messagesState.messages.forEach(messageData => usersSet.add(messageData.userId))
    return usersSet.size
  }

  //called in OwnMessage.tsx
  const editMessageHandler = (messageId: string, messageText: string) => {
    setInputtedMessage(messageText)
    setIsEditMessage(true)
    setEditMessageId(messageId)
  }

  //called in MessageInput.tsx
  const onMessageSend = () => {
    if (inputtedMessage && !isEditMessage) {
      dispatch({ type: ADD_NEW_MESSAGE, payload: { messageText: inputtedMessage } })
    } else if (inputtedMessage && isEditMessage) {
      dispatch({
        type: EDIT_MESSAGE,
        payload: { messageText: inputtedMessage, messageId: editMessageId },
      })
      setIsEditMessage(false)
    }
    setInputtedMessage("")
  }

  //deletes message and clears input if it was in editing mode
  const onDeleteMessage = (messageId: string) => {
    dispatch({ type: DELETE_MESSAGE, payload: { messageId: messageId } })
    setIsEditMessage(false)
    setInputtedMessage("")
  }

  if (!isLoaded) {
    return <Preloader />
  }

  const headerData = {
    participantsCount: getChatParticipants(),
    messagesCount: messagesState.messages.length,
    lastMessageDate: messagesState.messages[messagesState.messages.length - 1].createdAt,
  }
  return (
    <div className="chat">
      <Header headerData={headerData} />
      <MessageList
        allMessages={messagesState.messages}
        onDeleteMessage={onDeleteMessage}
        editMessageHandler={editMessageHandler}
        onLikeMessage={dispatch}
      />
      <MessageInput
        onMessageSend={onMessageSend}
        inputtedMessage={inputtedMessage}
        setInputtedMessage={setInputtedMessage}
      />
    </div>
  )
}

export default Chat
