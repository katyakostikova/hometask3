import React, { useState } from "react"
import { MessageData, MessagesAction } from "../../types"
import "./Message.css"
import LikeFilledImage from "../../assets/images/like-filled.svg"
import LikeImage from "../../assets/images/like-outlined.svg"
import { ADD_LIKE_ON_MESSAGE } from "../../constants/actions"

interface MessageProps {
  messageData: MessageData
  onLikeMessage: React.Dispatch<MessagesAction>
}

const Message = ({ messageData, onLikeMessage }: MessageProps) => {
  const [isLiked, setIsLiked] = useState(false)

  const likeButtonHandler = () => {
    setIsLiked(currentState => !currentState)
    onLikeMessage({type: ADD_LIKE_ON_MESSAGE, payload: {
      isLiked,
      messageId: messageData.id
    }})
  }

  const messageDate = messageData.editedAt
    ? new Date(messageData.editedAt)
    : new Date(messageData.createdAt)
  const hours =
    messageDate.getHours() < 10 ? `0${messageDate.getHours()}` : messageDate.getHours()
  const minutes =
    messageDate.getMinutes() < 10
      ? `0${messageDate.getMinutes()}`
      : messageDate.getMinutes()
  const messageDateString = messageData.editedAt
    ? `Edited at: ${hours}:${minutes}`
    : `${hours}:${minutes}`
  return (
    <li className={isLiked ? "message message-liked" : "message"}>
      <div className="message-user-avatar">
        <img src={messageData.avatar} alt="User avatar" />
      </div>
      <div className="message-content">
        <h3 className="message-user-name">{messageData.user}</h3>
        <p className="message-text">{messageData.text}</p>
        <p className="message-time">{messageDateString}</p>
      </div>
      <button className="message-like" onClick={likeButtonHandler}>
        <img src={isLiked ? LikeFilledImage : LikeImage} alt="Like Button" />
      </button>
    </li>
  )
}

export default Message
