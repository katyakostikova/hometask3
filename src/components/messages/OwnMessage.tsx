import React from "react"
import { MessageData, MessagesAction } from "../../types"
import "./OwnMessage.css"
import EditImage from "../../assets/images/edit.svg"
import DeleteImage from "../../assets/images/delete.svg"

interface OwnMessageProps {
  messageData: MessageData
  onDeleteMessage: (messageId: string) => void
  editMessageHandler: (messageId: string, messageText: string) => void
}

const OwnMessage = ({
  messageData,
  onDeleteMessage,
  editMessageHandler,
}: OwnMessageProps) => {
  const messageDate = messageData.editedAt
    ? new Date(messageData.editedAt)
    : new Date(messageData.createdAt)
  const hours =
    messageDate.getHours() < 10 ? `0${messageDate.getHours()}` : messageDate.getHours()
  const minutes =
    messageDate.getMinutes() < 10
      ? `0${messageDate.getMinutes()}`
      : messageDate.getMinutes()
  const messageDateString = messageData.editedAt
    ? `Edited at: ${hours}:${minutes}`
    : `${hours}:${minutes}`

  const deleteMessageHandler = () => {
    onDeleteMessage(messageData.id)
  }

  const onEditMessage = () => {
    editMessageHandler(messageData.id, messageData.text)
  }

  return (
    <li className="own-message">
      <div className="own-message-content">
        <p className="message-text">{messageData.text}</p>
        <p className="message-time">{messageDateString}</p>
      </div>
      <div className="message-buttons-container">
        <button className="message-edit" onClick={onEditMessage}>
          <img src={EditImage} alt="Edit Message Button" />
        </button>
        <button className="message-delete" onClick={deleteMessageHandler}>
          <img src={DeleteImage} alt="Delete Message Button" />
        </button>
      </div>
    </li>
  )
}

export default OwnMessage
