import React from "react"
import "./MessageInput.css"

interface MessageInputProps {
  onMessageSend: () => void
  inputtedMessage: string
  setInputtedMessage: React.Dispatch<React.SetStateAction<string>>
}

const MessageInput = ({
  onMessageSend,
  inputtedMessage,
  setInputtedMessage,
}: MessageInputProps) => {

  const submitHandler = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
  }

  return (
    <div className="message-input">
      <form onSubmit={(e) => submitHandler(e)}>
        <input
          type="text"
          className="message-input-text"
          value={inputtedMessage}
          onChange={e => setInputtedMessage(e.target.value)}
          placeholder="Enter new Message"
        />
        <button className="message-input-button" type="submit" onClick={onMessageSend}>
          Send
        </button>
      </form>
    </div>
  )
}

export default MessageInput
