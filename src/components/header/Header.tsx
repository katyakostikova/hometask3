import React from "react"
import "./Header.css"

interface HeaderProps {
  headerData: {
    participantsCount: number
    messagesCount: number
    lastMessageDate: string
  }
}

const Header = ({ headerData }: HeaderProps) => {
  const lastMessageDate = new Date(headerData.lastMessageDate)

  const hours =
    lastMessageDate.getHours() < 10
      ? `0${lastMessageDate.getHours()}`
      : lastMessageDate.getHours()

  const minutes =
    lastMessageDate.getMinutes() < 10
      ? `0${lastMessageDate.getMinutes()}`
      : lastMessageDate.getMinutes()

  const month =
    lastMessageDate.getMonth() + 1 < 10
      ? "0" + (lastMessageDate.getMonth() + 1)
      : lastMessageDate.getMonth() + 1

  const date =
    lastMessageDate.getDate() < 10
      ? "0" + lastMessageDate.getDate()
      : lastMessageDate.getDate()

  const lastMessageDateString = `${date}.${month}.${lastMessageDate.getFullYear()} ${hours}:${minutes}`

  return (
    <div className="header">
      <div className="header-info">
        <h3 className="header-title">Hometask Chat</h3>
        <h3 className="header-users-count">
          {headerData.participantsCount} participants
        </h3>
        <h3 className="header-messages-count">{headerData.messagesCount}</h3>
      </div>
      <h3 className="header-last-message-date">
        Last message at {lastMessageDateString}
      </h3>
    </div>
  )
}

export default Header
