import React, { useEffect, useRef } from "react"
import { CURRENT_USER_ID } from "../../constants/userId"
import { MessageData, MessagesAction } from "../../types"
import Message from "../messages/Message"
import "./MessageList.css"
import OwnMessage from "../messages/OwnMessage"
import { DAY_OF_WEEK, MONTHS, OLD_DATE } from "../../constants/date"
import Divider from "./divider/Divider"

interface MessageListProps {
  allMessages: MessageData[]
  onDeleteMessage: (messageId: string) => void
  editMessageHandler: (messageId: string, messageText: string) => void
  onLikeMessage: React.Dispatch<MessagesAction>
}

const checkDividerDate = (date: string) => {
  const currentDate = new Date()
  const messageDate = new Date(date)
  const currentDateString = `${currentDate.getDate()} ${currentDate.getMonth()}`
  const yesterdayDateString = `${currentDate.getDate() - 1} ${currentDate.getMonth()}`
  const messageDateString = `${messageDate.getDate()} ${messageDate.getMonth()}`

  switch (messageDateString) {
    case currentDateString:
      return "Today"
    case yesterdayDateString:
      return "Yesterday"
    default:
      const dayString = `${DAY_OF_WEEK.get(
        messageDate.getDay()
      )}, ${messageDate.getDate()} ${MONTHS.get(messageDate.getMonth())}`
      return dayString
  }
}

const checkIsDifferentDate = (firstDate: string, secondDate: string) => {
  if (firstDate && secondDate) {
    const firstDateParsed = new Date(firstDate)
    const secondDateParsed = new Date(secondDate)
    if (firstDateParsed.getDate() === secondDateParsed.getDate()) return false
    else return true
  }
}

const MessageList = ({
  allMessages,
  onDeleteMessage,
  editMessageHandler,
  onLikeMessage
}: MessageListProps) => {
  const messagesEndRef = useRef<HTMLOListElement | null>(null)

  //scroll messages to the last
  useEffect(() => {
    const lastChild = messagesEndRef.current!.lastElementChild
    lastChild?.scrollIntoView({ behavior: "smooth" })
  }, [allMessages])

  let prevMessageDate = OLD_DATE

  return (
    <ol ref={messagesEndRef} className="message-list">
      {allMessages.map(message => {
        const isDivided = checkIsDifferentDate(prevMessageDate, message.createdAt)
        const dividerDate = isDivided ? checkDividerDate(message.createdAt) : ""

        prevMessageDate = message.createdAt

        return (
          <div className={message.userId === CURRENT_USER_ID ? "own-message-container" : 'message-container'} key={message.id}>
            {isDivided && <Divider dayText={dividerDate} />}
            {message.userId === CURRENT_USER_ID ? (
              <OwnMessage
                messageData={message}
                editMessageHandler={editMessageHandler}
                onDeleteMessage={onDeleteMessage}
              />
            ) : (
              <Message messageData={message} onLikeMessage={onLikeMessage}/>
            )}
          </div>
        )
      })}
    </ol>
  )
}

export default MessageList
