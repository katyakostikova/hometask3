export interface MessageData {
  id: string
  userId: string
  avatar: string
  user: string
  text: string
  createdAt: string
  editedAt: string
  likedBy?: string[]
}

export type MessagesState = {
  messages: MessageData[] | []
}

export type MessagesAction = {
  type: string
  payload: {
    messages?: MessageData[] | [],
    messageText?: string,
    messageId?: string,
    isLiked?: boolean
  }
}