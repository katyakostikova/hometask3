import React from "react"
import "./App.css"
import Chat from "./components/chat/Chat"

const App = () => {
  return (
    <div className="App">
      <div className="logo">
        <img className="logo-img" src={require("./assets/images/logo.png")} />
        <h2>Binary Studio Academy hometask chat</h2>
      </div>
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
    </div>
  )
}

export default App
